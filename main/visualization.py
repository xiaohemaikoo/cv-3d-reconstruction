import open3d as o3d
import numpy as np
import open3d_tutorial as o3dtut


# visualization of point clouds.
# pcd = o3d.io.read_point_cloud('sparse.ply')
# o3d.visualization.draw_geometries([pcd])


def display_inlier_outlier(cloud, ind):
    inlier_cloud = cloud.select_by_index(ind)
    outlier_cloud = cloud.select_by_index(ind, invert=True)

    print("Showing outliers (red) and inliers (gray): ")
    outlier_cloud.paint_uniform_color([1, 0, 0])
    inlier_cloud.paint_uniform_color([0.8, 0.8, 0.8])
    visualization_pcds([inlier_cloud, outlier_cloud], point_size=3)

    # o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud],
    #                                   zoom=0.3412,
    #                                   front=[0.4257, -0.2125, -0.8795],
    #                                   lookat=[2.6172, 2.0475, 1.532],
    #                                   up=[-0.0694, -0.9768, 0.2024])


def visualization_pcds(pcds, point_size=1.5):
    vis = o3d.visualization.Visualizer()
    vis.create_window()

    render_option = vis.get_render_option()
    render_option.background_color = np.array([1, 1, 1])  # set background white
    render_option.point_size = point_size  # set point size
    for pcd in pcds:
        vis.add_geometry(pcd)
    vc = vis.get_view_control()
    vc.set_up([-0.0694, -0.9768, 0.2024])

    vis.run()
    vis.destroy_window()


def visualization_pcd(pcd):
    visualization_pcds([pcd])


def visualization(cloud_filename='sparse.ply', surface_reconstruction=False):
    pcd = o3d.io.read_point_cloud(cloud_filename)
    visualization_pcd(pcd)

    print("Statistical oulier removal")
    cl, ind = pcd.remove_statistical_outlier(nb_neighbors=400, std_ratio=1.0)
    display_inlier_outlier(pcd, ind)
    pcd = pcd.select_by_index(ind)
    visualization_pcd(pcd)
    #     o3d.visualization.draw_geometries([pcd])

    if not surface_reconstruction:
        return

    # This algorithm assumes that the PointCloud has normals. Require enough Cloud Points and with normals.
    # estimate_normals() can provide estimated normals, usually not sufficient
    print('run Poisson surface reconstruction')
    pcd.estimate_normals()
    with o3d.utility.VerbosityContextManager(
            o3d.utility.VerbosityLevel.Debug) as cm:
        mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(
            pcd, depth=9)
    print(mesh)
    o3d.visualization.draw_geometries([mesh],
                                      zoom=0.664,
                                      front=[-0.4761, -0.4698, -0.7434],
                                      lookat=[1.8900, 3.2596, 0.9284],
                                      up=[0.2304, -0.8825, 0.4101])

visualization()

"""
def display_inlier_outlier(cloud, ind):
    inlier_cloud = cloud.select_by_index(ind)
    outlier_cloud = cloud.select_by_index(ind, invert=True)

    print("Showing outliers (red) and inliers (gray): ")
    outlier_cloud.paint_uniform_color([1, 0, 0])
    inlier_cloud.paint_uniform_color([0.8, 0.8, 0.8])
    o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud],
                                      zoom=0.3412,
                                      front=[0.4257, -0.2125, -0.8795],
                                      lookat=[2.6172, 2.0475, 1.532],
                                      up=[-0.0694, -0.9768, 0.2024])


def visualization(cloud_filename='sparse.ply'):
    pcd = o3d.io.read_point_cloud(cloud_filename)
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    render_option = vis.get_render_option()	#设置点云渲染参数
    render_option.background_color = np.array([0.9, 0.9, 0.9])	#设置背景色（这里为黑色）
    render_option.point_size = 2.0	#设置渲染点的大小
    vis.add_geometry(pcd)
    vis.run()
    vis.destroy_window()

    print("Statistical oulier removal")
    cl, ind = pcd.remove_statistical_outlier(nb_neighbors=400, std_ratio=1.0)
    display_inlier_outlier(pcd, ind)
    pcd = pcd.select_by_index(ind)
    o3d.visualization.draw_geometries([pcd])

    # radii = [0.005, 0.01, 0.02, 0.04]
    # pcd.estimate_normals()
    # rec_mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(
    #     pcd, o3d.utility.DoubleVector(radii))
    # o3d.visualization.draw_geometries([pcd, rec_mesh])

    pcd.estimate_normals()
    print('run Poisson surface reconstruction')
    with o3d.utility.VerbosityContextManager(
            o3d.utility.VerbosityLevel.Debug) as cm:
        mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(
            pcd, depth=16)
    print(mesh)
    o3d.visualization.draw_geometries([mesh],
                                      zoom=0.664,
                                      front=[-0.4761, -0.4698, -0.7434],
                                      lookat=[1.8900, 3.2596, 0.9284],
                                      up=[0.2304, -0.8825, 0.4101])


# visualization()


def test01():
    # pcd = o3dtut.get_eagle_pcd()
    pcd = o3d.io.read_point_cloud('eagle.ply')
    print(pcd)
    o3d.visualization.draw_geometries([pcd],
                                      zoom=0.664,
                                      front=[-0.4761, -0.4698, -0.7434],
                                      lookat=[1.8900, 3.2596, 0.9284],
                                      up=[0.2304, -0.8825, 0.4101])

    print('run Poisson surface reconstruction')
    with o3d.utility.VerbosityContextManager(
            o3d.utility.VerbosityLevel.Debug) as cm:
        mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(
            pcd, depth=9)
    print(mesh)
    o3d.visualization.draw_geometries([mesh],
                                      zoom=0.664,
                                      front=[-0.4761, -0.4698, -0.7434],
                                      lookat=[1.8900, 3.2596, 0.9284],
                                      up=[0.2304, -0.8825, 0.4101])

"""
# test01()






