from helperFct import *
from skimage import io
from skimage.filters import gaussian
#from skimage.measure import block_reduce
#from skimage.transform import resize

class View:

    def __init__(self, img_path_):

        self.img_id = img_path_[img_path_.rfind('\\') + 1:img_path_.rfind('.')]
        #self.img_orig = cv.imread(img_path_)
        #self.img = cv.cvtColor(self.img_orig, cv.COLOR_BGR2GRAY)
        self.img_orig = io.imread(img_path_)
        self.img = cv.imread(img_path_)
        self.img_gauss = gaussian(self.img_orig, sigma=25, multichannel=True) * 255

        # downsample = 50
        # ds_array = self.img_orig/255
        # r = block_reduce(ds_array[:, :, 0],(downsample, downsample),np.mean,cval=0)
        # g = block_reduce(ds_array[:, :, 1],(downsample, downsample),np.mean,cval=0)
        # b = block_reduce(ds_array[:, :, 2],(downsample, downsample),np.mean,cval=0)
        # ds_array = np.stack((r, g, b), axis=-1)
        # self.img_gauss = resize(ds_array, self.img_orig.shape)

        self.keypoints = []
        self.descriptors = []
        self.R = np.zeros((3, 3), dtype=float)
        self.t = np.zeros((3, 1), dtype=float)
        

    def getFeatures(self, detector, extractor):

        if detector == extractor:
            self.keypoints, self.descriptors = detector.detectAndCompute(self.img, None)
        else:
            self.keypoints = detector.detect(self.img)
            self.keypoints, self.descriptors = extractor.compute(self.img, self.keypoints)


class Match:

    def __init__(self, view1_, view2_):

        self.indices1 = []
        self.indices2 = []
        self.distances = []
        self.img_id1 = view1_.img_id
        self.img_id2 = view2_.img_id
        self.inliers1 = []
        self.inliers2 = []
        self.view1 = view1_
        self.view2 = view2_
        

    def getMatches(self, matcher):

        matches = matcher.match(self.view1.descriptors, self.view2.descriptors)
        matches = sorted(matches, key=lambda x: x.distance)

        for i in range(len(matches)):
            self.indices1.append(matches[i].queryIdx)
            self.indices2.append(matches[i].trainIdx)
            self.distances.append(matches[i].distance)


class Base:

    def __init__(self, view1_, view2_, match_):

        self.view1 = view1_
        self.view2 = view2_
        self.match = match_
        
        self.view1.R = np.eye(3, 3)
        

    def getGeometry(self, K, Kinv):

        F = getFundamentalMat(self.view1, self.view2, self.match)
        E = K.T @ F @ K
        
        R, t = self.validateGeometry(E, K, Kinv)

        return R, t
    

    def validateGeometry(self, E, K, Kinv):

        R1, R2, t1, t2 = decomposeE(E)
        if not checkCoherentRotation(R1):
            R1, R2, t1, t2 = decomposeE(-E)

        rpj_err, pcloud = self.triangulatePoints(K, Kinv, R1, t1)
        if rpj_err > 100.0 or not testTriangulation(pcloud, np.hstack((R1, t1))):

            rpj_err, pcloud = self.triangulatePoints(K, Kinv, R1, t2)
            if rpj_err > 100.0 or not testTriangulation(pcloud, np.hstack((R1, t2))):

                rpj_err, pcloud = self.triangulatePoints(K, Kinv, R2, t1)
                if rpj_err > 100.0 or not testTriangulation(pcloud, np.hstack((R2, t1))):

                    return R2, t2

                else:
                    return R2, t1

            else:
                return R1, t2

        else:
            return R1, t1
        

    def triangulatePoints(self, K, Kinv, R, t):

        P1 = np.hstack((self.view1.R, self.view1.t))
        P2 = np.hstack((R, t))

        ppts1, ppts2 = getAlignedKeypoints(kpts1=self.view1.keypoints, idx_list1=self.match.inliers1,
                                           kpts2=self.view2.keypoints, idx_list2=self.match.inliers2)

        ppts1 = cv.convertPointsToHomogeneous(ppts1)[:, 0, :]
        ppts2 = cv.convertPointsToHomogeneous(ppts2)[:, 0, :]

        rpj_err = []
        pcloud = np.zeros((0, 3))

        for i in range(len(ppts1)):
            u1 = ppts1[i, :]
            u2 = ppts2[i, :]

            u1_normalized = Kinv.dot(u1)
            u2_normalized = Kinv.dot(u2)

            pt_3d = getPoint3D(u1_normalized, P1, u2_normalized, P2)

            error = computeRpjErr(pt_3d, u2[0:2], K, R, t)
            rpj_err.append(error)

            pcloud = np.concatenate((pcloud, pt_3d.T), axis=0)

        return np.mean(rpj_err), pcloud