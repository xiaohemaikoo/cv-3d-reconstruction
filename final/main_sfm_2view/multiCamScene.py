from helperClass import *
import os
import glob
import open3d as o3d

class MultiCamScene:
    
    def __init__(self):
        
        self.__features_matched = False
        self.__imgs_id = []
        self.__detectorFlag = 'sift'
        self.__extractorFlag = 'sift'
        self.__detector = cv.SIFT.create()
        self.__extractor = self.__detector
        self.__matcher = cv.BFMatcher(cv.NORM_L2, crossCheck=True)
        self.__views = []
        self.__matches = {}
        self.__pcloud = np.zeros((0, 3), dtype=np.float64)
        self.__pcloudRGB = np.zeros((0, 3), dtype=np.uint8)
        self.__pmap = {}
        self.__pcounter = 0
        self.__K = None
        self.__Kinv = None
        self.__ptErrors = []
        self.__errors = []
        self.__totalMeanErrors = []
        self.__done = []
        self.__root = None
        
    
    def loadData(self, path_, format_):
        
        print("Loading image files...")
        img_paths = sorted(glob.glob(os.path.join(path_, '*.' + format_)))
        for img_path in img_paths:
            self.__views.append(View(img_path))
            self.__imgs_id.append(self.__views[-1].img_id)
            
        for i in range(0, len(self.__views) - 1):
            for j in range(i+1, len(self.__views)):
                self.__matches[(self.__views[i].img_id, self.__views[j].img_id)] = Match(self.__views[i], self.__views[j])

        if os.path.exists(os.path.join(path_, 'K.txt')):
            print("Load K-matrix form K.txt")    
            self.__K = np.genfromtxt(os.path.join(path_, 'K.txt'))
        else:
            print("Could not find K.txt, use mock-up calibration")    
            img_size = self.__views[0].img.shape
            max_w_h = max(img_size)
            self.__K = np.array([[max_w_h, 0, img_size[1]/2.0],
                                [0, max_w_h, img_size[0]/2.0],
                                [0, 0, 1]])
        
        self.__Kinv = np.linalg.inv(self.__K)
        self.__root = path_
        
        
    def initFeatures(self):
        
        if self.__detectorFlag == self.__extractorFlag:
            print(f"Initializing using {self.__detectorFlag}")
        else:
            print(f"Initializing using {self.__detectorFlag} (detector) and {self.__extractorFlag} (extractor)")

        for view in self.__views:
            print(f"Initializing keypoints and descriptors for view {view.img_id}")
            view.getFeatures(self.__detector, self.__extractor)

        lastView = None
        for match in self.__matches.values():
            if lastView != match.view1.img_id:
                lastView = match.view1.img_id
                print(f"Matching featrues with view {lastView}")
            match.getMatches(self.__matcher)

        self.__features_matched = True
        
        
    def setFeatureType(self, detector, extractor=None):
        
        if detector == 'sift':
            self.__detector = cv.SIFT.create()
        elif detector == 'fastfeature':
            self.__detector = cv.FastFeatureDetector.create()
        elif detector == 'orb':
            self.__detector = cv.ORB.create(nfeatures=3000)
        elif detector == 'simpleblob':
            self.__detector = cv.SimpleBlobDetector.create()
        elif detector == 'kaze':
            self.__detector = cv.KAZE.create()
        elif detector == 'mser':
            self.__detector = cv.MSER.create()
        else:
            print("detector type not supported")
        self.__detectorFlag = detector
            
        if extractor == None:
            self.__extractor = self.__detector
            self.__extractorFlag = self.__detectorFlag
        else:
            if extractor == 'sift':
                self.__extractor = cv.SIFT.create()
            elif detector == 'fastfeature':
                self.__detector = cv.FastFeatureDetector.create()
            elif detector == 'orb':
                self.__detector = cv.ORB.create(nfeatures=3000)
            elif detector == 'simpleblob':
                self.__detector = cv.SimpleBlobDetector.create()
            elif detector == 'kaze':
                self.__detector = cv.KAZE.create()
            elif detector == 'mser':
                self.__detector = cv.MSER.create()
            else:
                print("extractor type not supported")
            self.__extractorFlag = extractor
        
        if extractor in ['sift', 'surf']:
            self.matcher = cv.BFMatcher(cv.NORM_L2, crossCheck=True)
        else:
            self.matcher = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)


    def getReprojectionErrors(self):
        
        return self.__errors


    def getTotalMeanErrors(self):
        
        return self.__totalMeanErrors


    def getImg(self, viewIdx=None):
        
        if viewIdx == None:
            imgs = []
            for view in self.__views:
                imgs.append(view.img_orig)
            return imgs
        
        else:
            return self.__views[viewIdx].img_orig


    def getPointCloud(self, flag='nparray'):

        if flag == 'nparray':
            return self.__pcloud, self.__pcloudRGB
        elif flag == 'open3d':
            pcd = o3d.geometry.PointCloud()
            pcd.points = o3d.utility.Vector3dVector(self.__pcloud)
            pcd.colors = o3d.utility.Vector3dVector(self.__pcloudRGB / 255)
            return pcd
            
            
    def __savePLY(self):

        number = len(self.__done)
        filename = os.path.join(self.__root, str(number) + '_views.ply')
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(self.__pcloud)
        # pcd.colors = o3d.utility.Vector3dVector(self.__pcloudRGB)
        pcd.colors = o3d.utility.Vector3dVector(self.__pcloudRGB / 255)
        o3d.io.write_point_cloud(filename, pcd)
        
        
    def showPointCloud(self, number=None, render_radius=None, rm_stat_outliers=False, nb_neighbors=400, std_ratio=1.0):
        
        if number == None:
            number = len(self.__done)
        elif number < 2:
            number = 2
        elif number > len(self.__done):
            number = len(self.__done)

        filename = os.path.join(self.__root, str(number) + '_views.ply')
        pcd = o3d.io.read_point_cloud(filename)

        if not render_radius == None:
            pcloud = np.asarray(pcd.points)
            center = np.mean(pcloud, axis=0)
            dif = pcloud - center
            dif = np.linalg.norm(dif, axis=1)
            idx = np.nonzero(dif <= render_radius)[0]
            pcd = pcd.select_by_index(idx)

        if rm_stat_outliers:
            _, ind = pcd.remove_statistical_outlier(nb_neighbors=nb_neighbors, std_ratio=std_ratio)
            pcd = pcd.select_by_index(ind)

        vis = o3d.visualization.Visualizer()
        vis.create_window()
        render_option = vis.get_render_option()
        render_option.background_color = np.array([0, 0, 0])
        render_option.point_size = 3.0
        vis.add_geometry(pcd)
        vis.run()
        vis.destroy_window()
        
        
    def __removeMappedPoints(self, match, img_idx):

        inliers1 = []
        inliers2 = []

        for i in range(len(match.inliers1)):
            if (img_idx, match.inliers1[i]) not in self.__pmap:
                inliers1.append(match.inliers1[i])
                inliers2.append(match.inliers2[i])

        match.inliers1 = inliers1
        match.inliers2 = inliers2
        
        
    def __triangulatePoints(self, view1, view2):

        P1 = np.hstack((view1.R, view1.t))
        P2 = np.hstack((view2.R, view2.t))

        match = self.__matches[(view1.img_id, view2.img_id)]
        ppts1, ppts2 = getAlignedKeypoints(view1.keypoints, match.inliers1,
                                           view2.keypoints, match.inliers2)
        if len(ppts1) == 0:
            return [0.], [0.]
        
        ppts1 = cv.convertPointsToHomogeneous(ppts1)[:, 0, :]
        ppts2 = cv.convertPointsToHomogeneous(ppts2)[:, 0, :]
        rpj_err1 = []
        rpj_err2 = []

        for i in range(len(ppts1)):

            u1 = ppts1[i, :]
            u2 = ppts2[i, :]

            u1_normalized = self.__Kinv.dot(u1)
            u2_normalized = self.__Kinv.dot(u2)

            pt_3d = getPoint3D(u1_normalized, P1, u2_normalized, P2)
            self.__pcloud = np.concatenate((self.__pcloud, pt_3d.T), axis=0)

            error1 = computeRpjErr(pt_3d, u1[0:2], self.__K, view1.R, view1.t)
            rpj_err1.append(error1)
            error2 = computeRpjErr(pt_3d, u2[0:2], self.__K, view2.R, view2.t)
            rpj_err2.append(error2)

            self.__pmap[(self.__imgs_id.index(view1.img_id), match.inliers1[i])] = self.__pcounter
            self.__pmap[(self.__imgs_id.index(view2.img_id), match.inliers2[i])] = self.__pcounter
            self.__pcounter += 1

        return rpj_err1, rpj_err2
    
    
    def __computeGeometryPnP(self, view):

        if self.__extractorFlag in ['sift', 'surf']:
            matcher = cv.BFMatcher(cv.NORM_L2, crossCheck=False)
        else:
            matcher = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=False)

        old_descriptors = []
        for old_view in self.__done:
            old_descriptors.append(old_view.descriptors)

        matcher.add(old_descriptors)
        matcher.train()
        matches = matcher.match(queryDescriptors=view.descriptors)
        pcloud = np.zeros((0, 3))
        pcloud_2d = np.zeros((0, 2))

        for match in matches:
            old_img_idx, new_img_kp_idx, old_img_kp_idx = match.imgIdx, match.queryIdx, match.trainIdx

            if (old_img_idx, old_img_kp_idx) in self.__pmap:

                pt_2d = np.array(view.keypoints[new_img_kp_idx].pt).T.reshape((1, 2))
                pcloud_2d = np.concatenate((pcloud_2d, pt_2d), axis=0)

                pt_3d = self.__pcloud[self.__pmap[(old_img_idx, old_img_kp_idx)], :].T.reshape((1, 3))
                pcloud = np.concatenate((pcloud, pt_3d), axis=0)

        _, R, t, _ = cv.solvePnPRansac(pcloud[:, np.newaxis], pcloud_2d[:, np.newaxis], self.__K, None,
                                       confidence=0.99, reprojectionError=8.0, flags=cv.SOLVEPNP_DLS)
        R, _ = cv.Rodrigues(R)
        return R, t
        
        
    def __computeGeometry(self, view1, view2=None, isBase=False):

        if isBase and view2:

            match = self.__matches[(view1.img_id, view2.img_id)]
            base = Base(view1, view2, match)
            view2.R, view2.t = base.getGeometry(self.__K, self.__Kinv)

            rpj_err1, rpj_err2 = self.__triangulatePoints(view1, view2)
            self.__errors.append(np.mean(rpj_err1))
            self.__errors.append(np.mean(rpj_err2))

            self.__ptErrors.append(rpj_err1)
            self.__ptErrors.append(rpj_err2)

            self.__done.append(view1)
            self.__done.append(view2)

        else:

            view1.R, view1.t = self.__computeGeometryPnP(view1)
            errors = []

            for i, old_view in enumerate(self.__done):

                match = self.__matches[(old_view.img_id, view1.img_id)]
                F = getFundamentalMat(old_view, view1, match)
                self.__removeMappedPoints(match, i)
                _, rpj_err = self.__triangulatePoints(old_view, view1)
                errors += rpj_err

            self.__errors.append(np.mean(errors))
            self.__ptErrors.append(errors)

            self.__done.append(view1)

        self.__totalMeanErrors.append(np.mean(self.__errors))
            
            
    def __computeRGB(self):
        
        #rgb = np.empty((3,2,3), dtype=np.uint8)
        rgb = [[] for i in range(len(self.__pcloud))]
        
        for key, val in self.__pmap.items():
            viewIdx, kptIdx = key
            kpt = self.__views[viewIdx].keypoints[kptIdx]
            img_pt = kpt.pt
            rgb_pt = self.__views[viewIdx].img_gauss[int(img_pt[1]), int(img_pt[0])]
            rgb[val].append(rgb_pt)
            
        for col in rgb:
            if len(col) == 1:
                col.append(col[0])
            
        rgb = np.asarray(rgb, dtype=np.uint8)
        self.__pcloudRGB = np.mean(rgb, axis=1, dtype=np.uint8)
            
            
    def reconstruct(self, view_count=None):

        if self.__features_matched == False:
            print("load data and initialise features first")
            return
        
        print("Reconstructing base view")
        baseView1, baseView2 = self.__views[0], self.__views[1]
        self.__computeGeometry(baseView1, baseView2, isBase=True)
        self.__computeRGB()
        print(f"...mean reprojection error {self.__totalMeanErrors[-1]}")
        self.__savePLY()

        if view_count == None:
            view_count = len(self.__views)
        elif view_count < 2:
            view_count = 2
        elif view_count > len(self.__views):
            view_count = len(self.__views)

        for i in range(2, view_count):
            print(f"Adding reconstruction of {self.__views[i].img_id}")
            self.__computeGeometry(self.__views[i])
            self.__computeRGB()
            print(f"...mean reprojection error {self.__totalMeanErrors[-1]}")
            self.__savePLY()