import numpy as np
import cv2 as cv

def getAlignedKeypoints(kpts1, idx_list1, kpts2, idx_list2):

    pts1 = np.array([kp.pt for kp in kpts1])[idx_list1]
    pts2 = np.array([kp.pt for kp in kpts2])[idx_list2]
    return pts1, pts2


def getFundamentalMat(view1, view2, match):

    ppts1, ppts2 = getAlignedKeypoints(view1.keypoints, match.indices1,
                                       view2.keypoints, match.indices2)
    F, mask = cv.findFundamentalMat(ppts1, ppts2, method=cv.FM_RANSAC, ransacReprojThreshold=0.9, confidence=0.99)
    mask = mask.astype(bool).flatten()
    match.inliers1 = np.array(match.indices1)[mask]
    match.inliers2 = np.array(match.indices2)[mask]

    return F


def testTriangulation(pts, P):

    P = np.vstack((P, np.array([0, 0, 0, 1])))
    rpj_pts = cv.perspectiveTransform(pts[np.newaxis], P)
    z = rpj_pts[0, :, -1]
    if (np.sum(z > 0)/z.shape[0]) < 0.75:
        return False
    else:
        return True


def checkCoherentRotation(R):
    if np.linalg.det(R) + 1.0 < 1e-7:
        return False
    else:
        return True
    
    
def decomposeE(E):

    W = np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])
    u, w, vt = np.linalg.svd(E)

    R1 = u @ W @ vt
    R2 = u @ W.T @ vt
    t1 = u[:, -1].reshape((3, 1))
    t2 = - t1
    
    return R1, R2, t1, t2


def getPoint3D(u1, P1, u2, P2):

    A = np.array([[u1[0] * P1[2, 0] - P1[0, 0], u1[0] * P1[2, 1] - P1[0, 1], u1[0] * P1[2, 2] - P1[0, 2]],
                  [u1[1] * P1[2, 0] - P1[1, 0], u1[1] * P1[2, 1] - P1[1, 1], u1[1] * P1[2, 2] - P1[1, 2]],
                  [u2[0] * P2[2, 0] - P2[0, 0], u2[0] * P2[2, 1] - P2[0, 1], u2[0] * P2[2, 2] - P2[0, 2]],
                  [u2[1] * P2[2, 0] - P2[1, 0], u2[1] * P2[2, 1] - P2[1, 1], u2[1] * P2[2, 2] - P2[1, 2]]])

    B = np.array([-(u1[0] * P1[2, 3] - P1[0, 3]),
                  -(u1[1] * P1[2, 3] - P1[1, 3]),
                  -(u2[0] * P2[2, 3] - P2[0, 3]),
                  -(u2[1] * P2[2, 3] - P2[1, 3])])

    X = cv.solve(A, B, flags=cv.DECOMP_SVD)
    
    return X[1]


def computeRpjErr(pt_3d, pt_2d, K, R, t):

    rpj_pt = K @ (R @ pt_3d + t)
    rpj_pt = cv.convertPointsFromHomogeneous(rpj_pt.T)[:, 0, :].T
    error = np.linalg.norm(pt_2d.reshape((2, 1)) - rpj_pt)
    return error