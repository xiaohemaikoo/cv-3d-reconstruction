3D Reconstruction using Structure from Motion (SfM) pipeline with OpenGL visualization on C++
https://capsulesbot.com/blog/2019/03/12/apolloscape-sfm.html


https://github.com/Kush0301/Structure-from-Motion

##### Useful, contain most common 3D reconstruction approaches
https://github.com/openMVG/awesome_3DReconstruction_list


OpenSfM // open source SfM library project
https://github.com/mapillary/OpenSfM/



A tutorial about implementing sift in python using opencv
https://github.com/rmislam/PythonSIFT



C++ SfM code from: Mastering OpenCV with Practical Computer Vision Projects
https://github.com/MasteringOpenCV/code/tree/master/Chapter4_StructureFromMotion


Class Project: 3D reconstruction from two unrectified images
https://github.com/samehkhamis/3DReconstruction/blob/master/calibration.py















