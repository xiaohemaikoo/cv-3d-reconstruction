% \documentclass[review]{cvpr}
\documentclass[final]{cvpr}

\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,colorlinks,bookmarks=false]{hyperref}

\usepackage{graphicx}
\usepackage{subfigure}

% \def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
% \def\confYear{CVPR 2021}
% \setcounter{page}{4321} % For final version only


\begin{document}

%%%%%%%%% TITLE
\title{Sparse 3D Reconstruction using Structure from Motion (SfM)}

\author{Omar Al-Bazaz \\
3464669 \\
Physics
\and
Xianghe Ma \\
3690701 \\
Scientific Computing
\and
Lingyi Zhou \\
3691771 \\
Scientific Computing
}

\maketitle


%%%%%%%%% ABSTRACT
\begin{abstract}
  3D reconstruction is a widely studied topic in the field of computer vision. In our project, we consider the 2-view reconstruction
  and multi-view reconstruction using Structure from Motion (SfM). SfM is a classic method including 5 main steps:
  feature points detection and matching, estimating fundamental matrix, camera pose estimation, triangulation and bundle adjustment.
  We consider both calibrated camera and uncalibrated camera. In the case of calibrated camera, the intrinsic camera matrix K is known so that we can make use of it to compute camera matrix P easily. The case of uncalibrated camera is much more complex since we need to estimate intrinsic camera matrix K. In this report, we first describe the approach and then show the results.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
  3D reconstruction refers to a mathematical process and computer technology that uses a 2D projection or image to reconstruct the 3D information (for example, shape) of an object. It is the reverse process of obtaining 2D images from 3D scenes. An image is a projection from a 3D scene onto a 2D plane. In this process, the depth is lost and the 3D point corresponding to a specific image point is constrained to be on the line of sight. If at least two images are available, a 3D point can be found as the intersection of two projection rays. \cite{wiki3D} In recent years, 3D reconstruction has a wide range of applications such as smart home, cultural relic virtual restoration, gaming and autopilot.

  Structure from Motion (SfM) is used in our project to reconstruct 3D scene. Structure from motion (SfM) is a photogrammetric range imaging technique for estimating 3D structures from 2D image sequences that may be coupled with local motion signals. The principle is from human perceiving a 3D object by moving around the object and getting the depth information to generate the accurate 3D scene. \cite{wikiSfM} There are 5 steps in this approach. First, we use sift algorithm to detect key points and then match these features. Second, we estimate the fundamental matrix F with RANSAC algorithm. The third step is perspective-n-point (PnP) and there are two cases. If images are from a calibrated camera, which means we know the intrinsic matrix K, we use keypoints and K to compute the extrinsic parameters. If images are from a uncalibrated camera, we estimate intrinsic camera matrix K using Kruppa's equations and Sturm's method (self-calibration)\cite{Strum}. The fourth step is triangulation. The last step is bundle adjustment (BA) and the common methods to solve BA are Gauss-Newton algorithm and Levenberg–Marquardt algorithm.

  In our project we consider 2-view reconstruction and multi-view reconstruction. 2-view reconstruction is a simplified version of multi-view reconstruction. We only use 2 images and omit the BA step. Comparing the results of 2-view and multi-view we can see the effectiveness of more images and BA.

  The remaining parts of this report are formulated as follows. Section 2 is about related work. In section 3, we present detailed steps of SfM to 3D reconstruction. And we show the experiments and results in section 4. The last section is conclusion.



%------------------------------------------------------------------------
\section{Related Work}
  3D reconstruction is a popular topic and there are many different approachs to solve this problem. SfM is a classic algorithm and has many successful applications. For example, Photo tourism is a system for browsing large collections of photographs in 3D. The input is large collections of images from personal photo collections or Internet sites. The user can interactively move about the 3D space through the photo explorer interface \cite{tourism}.

  In recent years there are many new approaches such as KinectFusion. With KinectFusion algorithm, 3D models can be built in real-time without the need for RGB maps but only with depth maps. The KinectFusion algorithm was the first to realize real-time rigid body reconstruction based on cheap consumer cameras. It was a very influential work at the time, and it greatly promoted the commercialization of real-time dense 3D reconstruction.

  It is also possible to estimate 3D geometry from a single image. And it is considerably more difficult since depth cannot be estimated from pixel correspondences. Thus, further prior knowledge or user input is needed in order to recover or infer depth information. A recent paper infer from an image silhouette by specifying only the volume of the object by assuming plane symmetry and surface smoothness. \cite{singleImg}

  There are some open-source libraries for 3D reconstruction. OpenCV is a huge library supporting a wide range of algorithms related to computer vision and machine learning. OpenCV supports mainstream programming languages such as c++, python and java. We use OpenCV-Python in our code. OpenCV-Python makes use of Numpy making it easier to integrate with other libraries that use Numpy such as SciPy and Matplotlib. All the OpenCV array structures are converted to and from Numpy arrays. \cite{OpenCV-Python} For dense 3D reconstruction, CMVS and PMVS packages are useful.



%------------------------------------------------------------------------
\section{Technical Approach}

\subsection{Datasets}
  For 2-view version we download from internet a dataset with only 2 images. Intrinsic matrix K is known and we use it in our code.
  For multi-view reconstruction we download a dataset with dozens of photos. Also, intrinsic matrix K is known for this dataset. In our code we use our self-calibrated K from Sturm's method rather than the true K.

\subsection{Feature Detection and Matching}
  We use sift algorithm for feature detection. Sift algorithm consists of 4 steps: Scale-space peak selection, keypoint localization, orientation assignment and keypoint descriptor. For feature matching, the common methods are brute-force matching and FLANN matching. Brute-force matching takes the descriptor of one feature in first set and is matched with all other features in second set using some distance calculation. And the closest one is returned. \cite{cvMatching} Fast Library for Approximate Nearest Neighbors (FLANN) contains algorithms optimized for fast nearest neighbor search in large datasets and for high dimensional features. \cite{cvMatching}

  Scale-space peak selection is about creating a Gaussian difference pyramid and finding keypoints. We generate several octaves of the original image and the image's size in each octave is half of the previous one. Within each octave, images are progressively blurred with Gaussian kernel. Within each octave, we get the difference of two adjacent blurred images. The new image dataset is called Difference of Gaussians (DoG). To determine a keypoint, a pixel in an image is compared with its 8 neighbors as well as 9 pixels in the next scale and 9 pixels in previous scales. \cite{sift}

  Some keypoints from above step do not have enough contrast or influenced by noise. The goal of keypoint localization is to get rid of these points. Taylor series expansion of scale space is used to get a more accurate location of extrema. In the paper the threshold of intensity to reject a keypoint is set to 0.03. \cite{sift} And a 2x2 Hessian matrix for computing the principal curvature is used to remove edges.

  After the above steps, we have determined some gray value extreme points. Next, we need to determine the orientation of these extreme points. The problem to be solved in assigning orientation information to keypoints is to make keypoints invariant to image angle and rotation. The assignment of directions is achieved by finding the gradient of each extreme point.

  The last step of sift is computing the descriptor. For each keypoint, a 16x16 window around the point is taken and it is divided into 16 sub-blocks of 4x4 size. For each sub-block, an 8 bin orientation histogram is created.

  In our project, we use cv2.xfeatures2d.SIFT$\_$create() from OpenCV-Python to get sift. Then we use the function sift.detectAndCompute(image, None) to get keypoints and descriptors of input image. For matching we use FLANN matching. With command flann.knnMatch(parameters) we get the matches. Input parameters are descriptors of two images.

\subsection{Fundamental Matrix and RANSAC}
  Fundamental matrix F is a 3×3 matrix that expresses the correspondence between the two image points of the 3D point. In epipolar geometry, for a pair of image points taking from two camera poses and corresponds to the same 3D point, their homogeneous image coordinates are $x_0$ and $x_1$. And fundamental matrix F satisfies $ {x_0}^T F x_1 = 0$.

  From the lecture we know F can be computed from 8 point algorithm. The steps of 8 point algorithm are presented here.
  \begin{itemize}
    \item Taking 8 points
    \item Compute T and condition points $\bar{x} = Tx$, $\bar{x}^\prime = T^\prime x^\prime$
    \item Assemble A with Af = 0 (f is vectorized F)
    \item Use SVD to compute $f^\ast = argmin {\|Af\|}_2$ subject to ${\|f\|}_2 = 1$
    \item Get F of unconditioned points
    \item make F rank 2
  \end{itemize}

  The disadvantage of 8 point algorithm is the result of F relies on the selection of 8 points. So, we need RANSAC algorithm to iteratively minimize the average geometric error. The result then becomes more stable.

  In our project we mainly use fundamental matrix function cv2.findFundamentalMat(parameters) (with method RANSAC) from OpenCV. And we also test our implementation of finding fundametal matrix with RANSAC. Although it is not as fast as OpenCV, it works the similar results.

\subsection{Self-Calibration}
  If the camera is calibrated, which means we know intrinsic matrix K, we can use keypoints found in the first step and K to compute essential matrix E using cv2.findEssentialMat(parameters) easily. If the camera is uncalibrated, we use self-calibration method to compute K and E. We use Strum's method based on the paper \cite{Strum}.
  This equation will be used in the algorithm (f is focal length. a and b are singular values. u and v are from matrix U and V): $ f^4 (a^2 (1 - {u_13}^2) (1 - {v_13}^2) - b^2 (1 - {u_23}^2) (1 - {v_23}^2)) + f^2 (a^2 ({u_13}^2 + {v_13}^2 - 2 {u_13}^2 {v_13}^2) - b^2 ({u_23}^2 + {v_23}^2 - 2 {u_23}^2 {v_23}^2)) + (a^2 {u_13}^2 {v_13}^2 - b^2 {u_23}^2 {v_23}^2) = 0 $

  The algorithm looks as follows:
  \begin{itemize}
    \item Compute the semi-calibrated intrinsic matrix K with width and height of the image
    \item Compute essential matrix E with equation $E = K^T F K$ and scale E to unit Frobenius norm
    \item Applying SVD to E ($E = U \Sigma V$) and extract the coefficients as well as the non-zero singular values
    \item Solve the equation described above and estimate focal length f
    \item Use the new focal length to get intrinsic matrix K
    \item Use the new intrinsic matrix K to recompute E
  \end{itemize}

  Our code is suitable for both cases. In the calibrated case, we use command cv2.findEssentialMat(parameters) to compute essential matrix E. Parameters include keypoints and K. And we choose method RANSAC. In the uncalibrated case, we use the algorithm described above to estimate K and E. In our code we use variable ``calibrated'' to switch between these two cases. The default setting is uncalibrated camera (calibrated = False).

\subsection{Pose Estimation}
  Before pose estimation, we use triangulation to get 3D points from keypoints. Now we have a set of 3D points in the world, their 2D projections in the image and the intrinsic parameter, the 6 DOF camera pose can be estimated. \cite{SfM} This problem is called Perspective-n-Point (PnP). A common method is linear least squares. PnP is prone to error as there are outliers in the given set of point correspondences. To overcome this error, we can use RANSAC \cite{SfM}.

  In our code, we use OpenCV command cv2.solvePnPRansac(parameters). We introduce some main parameters. \cite{opencv-docs}
  \begin{itemize}
    \item object points: array of 3D points
    \item image points: array of corresponding image points
    \item intrinsic matrix K
    \item flags: methods for solving PnP, we choose iterative method in our code
  \end{itemize}

\subsection{Triangulation}
  Triangulation is the process of determining a point in 3D space, given its projections onto two or more images. It is also called reconstruction. A common algorithm to solve this problem is Direct Linear Transformation (DLT). DLT is used to solve An ordinary system of linear equations $X = AY$ in the presence of noise. In the standard case, $ A = X Y^T (Y T^T)^(-1) $. What makes the direct linear transformation problem distinct from the  standard case is the fact that the left and right sides of the defining equation can differ by an unknown multiplicative factor \cite{wikiTri}. SVD is used in this algorithm.

  In our code we use cv2.triangulatePoints(parameters). Input parameters include 2D points and camera matrices P of each image. The output is an array of computed 3D points. This command uses DLT method.

\subsection{Bundle Adjustment}
Bundle adjustment is the problem of refining a visual reconstruction to produce jointly optimal
3D structure and viewing parameter (camera pose and/or calibration) estimates \cite{thesis_ba}. Bundle adjustment (BA) problem can be formulated as follows: Given a set of images depicting a number of 3D points from different viewpoints, BA simultaneously refines the 3D coordinates describing the scene geometry as well as the parameters of the relative motion and the optical characteristics of the camera employed to acquire the images \cite{BA}. It is a non-linear optimization problem. Methods to solve BA problem include least-squares and Levenberg–Marquardt algorithm. It minimizes the total reprojection error with respect to all 3D point $X_j$ and camera parameters $M_i$, and is illustrated by the following general equation and figure \ref{3.1}:

\begin{align}
\min_{M_i,X_j} E(M,X) = \sum_{i=1}^m \sum_{j=1}^n D(x_{ij}, M_i X_j)^2
\end{align}

\begin{figure}[htb]
\centering
\includegraphics[width=5.3cm,height=3.5cm]{ba.png}
\caption{Bundle adjustment.}
\label{3.1}
\end{figure}

In our code we use least-squares function from scipy. And we use method trust region reflective (trf). This algorithm iteratively solves trust-region subproblems augmented by a special diagonal quadratic term and with trust-region shape determined by the distance from the bounds and the direction of the gradient \cite{scipy-ls}. It works quite robust in unbounded and bounded problems and is particularly suitable for large sparse problems with bounds by using sparse Jacobians, thus it is the default algorithm. \cite{sparse_ba}



\section{Experiments and Results}

Even though the 2-view version is meant to be used for only two images it can actually provide a 3D-reconstructed scene for multiple views. This is achieved by simply triangulating a new view with respect to the last view added to the scene. This gives us the opportunity to not only compare 2-views between the different versions, but also multi-view scenes with and without the use of bundle adjustment. In both cases the reconstructions provide a 3D scene with corresponding reprojection errors. Visualization of the resulting point clouds is done with the python Open3D library, which has the advantage of using an interactive renderer. For comparison we mainly use two datasets, one of a fountain scene consisting of 11 images and one of a statue scene consisting of 57 images. Both scenes are provided with the intrinsic matrix K.

\subsection{Two-view}

To provide first insight on the resulting reconstruction we use the fountain scene, which consists of eleven 3072x2048 RGB images. One of those images is shown in Figure \ref{fountain}.

\begin{figure}[htb]
\centering
\includegraphics[width=6cm,height=4cm]{fountain.jpg}
\caption{One image from the fountain dataset.}
\label{fountain}
\end{figure}

Using the aforementioned SfM techniques we reconstructed the fountain scene using the provided K-matrix by incrementally adding out view after the other, basically stitching to 2-views reconstructions together. Each triangulated point is thus either visible from one or two views and not more. To attain the RGB colors for the points the mean RGB values are taken from the matched views, while the color images were first smoothed out via Gaussian filtering. The resulting point clouds then underwent a refinement process. This process is done by excluding points outside of a mass-centered sphere to get rid of clustered artifacts. Afterwards the remaining points then underwent a statistical reduction using the corresponding tools provided by Open3D. Thus the scenes are visualized. The result for two, five, eight and eleven views are captured in Figure \ref{f_recon}.

\begin{figure}[htb]
\centering
\subfigure[2 views]{
\label{f_recon_2}
\includegraphics[width=4cm,height=4cm]{fountain_2v_clean_gray.png}}
\subfigure[5 views]{
\label{f_recon_5}
\includegraphics[width=4cm,height=4cm]{fountain_5v_clean_gray.png}}
            % this blank line is import for 2x2 subfigures, or replace it by \quad
\subfigure[8 views]{
\label{f_recon_8}
\includegraphics[width=4cm,height=4cm]{fountain_8v_clean_gray.png}}
\subfigure[11 views]{
\label{f_recon_11}
\includegraphics[width=4cm,height=4cm]{fountain_11v_clean_gray.png}}

\caption{The figures show 3D reconstruction of the fountain scene after refinement for different sets of views. (a) Using only two views of the fountain. (b) Using the first 5 views of the dataset. (c) Using eight views. (d) Using all eleven views of the dataset.}
\label{f_recon}
\end{figure}

Looking at these result we notice that the amount of points is increasing with additional views or images, which is to be expected. However, this does not necessarily result in better reconstructions. There is little to complain about the 5-view and 8-view reconstruction, these do in fact seems like more complete reconstruction compared to 2-views. The 11-view result, which uses the full dataset, on the other hand seems to have brought some unwanted artifacts or errors into the scene. This suggest that scenes with a larger database are not suitable cases for this SfM implementation. That being said it is more a question about the difference in the viewing positions and angels rather than the amount of viewing points. To get a better understanding we show the number of triangulated points with respect to the number of views used in reconstruction in figure \ref{f_pt_num}. Further we plotted the mean reprojection error for each view in figure \ref{f_err}.

\begin{figure}[htb]
\centering
\includegraphics[width=7cm,height=5cm]{pcloud_size.png}
\caption{Plotting number of points in reconstruction scene (y-axis) against number of views (x-axis). This is done for the unrefined and refined point clouds.}
\label{f_pt_num}
\end{figure}

\begin{figure}[htb]
\centering
\includegraphics[width=7cm,height=5cm]{mean_rpj_err.png}
\caption{Showing the mean reprojection error for each view in logarithmic scale. This is done for the unrefined and refined point clouds.}
\label{f_err}
\end{figure}

Figure \ref{f_pt_num} shows that the amount of points in the scene is increasing linearly with respect to the number of views. This is indeed to be expected since we are dealing with a rather symmetric scene which should amount to similar number of features for each view. There is thus in principle no preferred view, which might have explained the errors in the 11-view result. Also, it seems that the refinement is not quite effective as hopped or might not be necessary as initially thought. This will become even more apparent when looking at figure \ref{f_err}. For the most part the reprojection error is under 10. Even though this might not be a satisfying result it reflects itself quite well in the reconstructed scenes when compared to the last two views, which cross the 100 and 1000 mark. We also found that the respective standard deviation are quite large. Taking this into account the artifacts in the 11-view scene are possibly due to bad fundamental matrices and the corresponding in- and outliers.


\subsection{Multi-view}

The dataset for multi-view 3D reconstruction contains 57 images. One of the 57 images of the statue is illustrated in Figure \ref{4.1}. Each image is a high resolution image with 1296x1936 RGB pixels. The other information of images is in Table \ref{a}. The image parameters can also be read from its Exif information. Since it is costly on processing high quality images, we use the down sample images as well the down sample intrinsic camera matrix to speed up the algorithm.

\begin{figure}[htb]
\centering
\includegraphics[width=4cm,height=6cm]{Figure_0.png}
\caption{One of the source image.}
\label{4.1}
\end{figure}

\begin{table}
\centering
\caption{Image information for multi-view dataset}
\begin{tabular}{cccccc}

     Camera & focal length & Resolution & aperture \\
     \hline
     NAKON D60 & 30mm & 300dpi & f/7.1 \\

\end{tabular}
\label{a}
\end{table}

To reconstruct the structure of this statue, we firstly apply SfM algorithm on a calibrated camera. Usually we can construct the intrinsic camera parameters by reading Exif information and camera models, for example, SLR cameras or mobile phone cameras. It is convenient to construct 3D structure if we can get all needed intrinsic parameters from those information. Figure \ref{4.2} shows the 3D reconstruction result based on calibrated camera. The SfM algorithm can not distinguish feature points of target object (eg. the statue) and feature points of background (eg. the wall, the street lamps). Therefore we use statistical oulier finding method from Open3D to remove the point clouds which are not the statue, see figure \ref{Fig.sub.2} and \ref{Fig.sub.3}.

\begin{figure}[h]
\centering
\subfigure[reconstructed statue point clouds]{
\label{Fig.sub.1}
\includegraphics[width=4.5cm,height=2.9cm]{Figure_1.png}}

\subfigure[plotting outliers (red points).]{
\label{Fig.sub.2}
\includegraphics[width=4.5cm,height=2.9cm]{Figure_2.png}}

\subfigure[statue point clouds without outliers]{
\label{Fig.sub.3}
\includegraphics[width=4.5cm,height=3.5cm]{Figure_3.png}}

\caption{The figures show the 3D reconstruction of a statue from calibrated camera. Figure (a) is the original point clouds reconstructed from SfM method. Figure (b) distinguishes 3D outliers(red points) by Open3D statistical oulier finding method. Figure (c) is the reconstruction without outliers regards to the statue.}
\label{4.2}
\end{figure}

For uncalibrated camera, the images' Exif information are usually missing and camera model is not known. We should estimate the intrinsics at first and then estimate extrinsic parameters. Therefore we assume that we do not know the intrinsic camera parameters and using Kruppa’s equation to compute an approximated intrinsic matrix from fundamental matrix and images size. Figure \ref{4.3} shows the 3D reconstruction result based on uncalibrated camera. We also remove outliers, see figure \ref{Fig.sub.5} and \ref{Fig.sub.6}. However approximated parameters will bring more errors to the point clouds, we apply bundle adjustment to minimize residuals of reprojection errors and get local minima status, see figure \ref{4.4}. We see much better picture of residuals, with the mean being very close to zero after optimization.

Our experiments shows that bundle adjustment algorithm can improve the accuracy of SfM algorithm on uncalibrated camera. When we compute fundamental matrix via OpenCV interface, the bundle adjustment doesn't change final results significantly. Since on most of time the OpenCV interface return a relatively precise fundamental matrix, thus we can get a good estimation of intrinsics. But sometimes our implementation of finding fundamental matrix from RANSAC is not as precise as the OpenCV, in this situation, we see bundle adjustment make the final results indeed better. But in our experiments, we see the bundle adjustment algorithm is very sensitive to outliers. When outliers give large errors, to minimize overall residuals the bundle adjustment will get even worse results. However we should not consider to much outliers in bundle adjustment, that's a problem. For example, we can see from figure \ref{4.5} there are many outliers give large errors before optimization when we try to find global optimal to our reconstruction, and this will cause the bundle adjustment gives unpredictable results. Usually we should remove as much outliers as possible to apply bundle adjustment, it will help to improve reconstructed figure, but strict outlier removal method will lead less point clouds.

\begin{figure}[h]
\centering
\subfigure[reconstructed statue point clouds]{
\label{Fig.sub.4}
\includegraphics[width=6cm,height=3cm]{Figure_4.png}}

\subfigure[plotting outliers (red points).]{
\label{Fig.sub.5}
\includegraphics[width=6cm,height=3cm]{Figure_5.png}}

\subfigure[statue point clouds without outliers]{
\label{Fig.sub.6}
\includegraphics[width=3.5cm,height=3.5cm]{Figure_6.png}}

\caption{The figures show the 3D reconstruction of a statue from uncalibrated camera. Figure (a) is the original point clouds reconstructed from SfM method. Figure (b) distinguishes 3D outliers(red points) by Open3D statistical outlier finding method. Figure (c) is the reconstruction without outliers regards to the statue.}
\label{4.3}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=5.5cm,height=4cm]{ba_1.png}
\caption{Bundle adjustment for local minima. The x axis is feature points, y axis is residuals. The blue figure represent residuals before optimization, the orange figure represent residuals after bundle adjustment optimization.}
\label{4.4}
\end{figure}


\begin{figure}[h]
\centering
\includegraphics[width=5.5cm,height=4cm]{ba_2.png}
\caption{Bundle adjustment for global minima. The x axis is feature points, y axis is residuals. The blue figure represent residuals before optimization, the orange figure represent residuals after bundle adjustment optimization.}
\label{4.5}
\end{figure}


\subsection{Comparison}

After discussing both SfM implementations separately we want to compare their quality. For this purpose we first add some details on the reconstruction of the statue scene by the 2-view implementation. Figure \ref{s_recon} shows the 3D-reconstruction of the statue for 40 and 57 views. For 40 views we also included the unrefined point cloud and the one exuding clustered outliers.

\begin{figure}[htb]
\centering
\subfigure[40 views (unrefined)]{
\label{s_recon_40ur}
\includegraphics[width=4cm,height=4cm]{statue_40v_raw.png}}
\subfigure[40 views (with stat. outliers)]{
\label{s_recon_40urr}
\includegraphics[width=4cm,height=4cm]{statue_40v_rad.png}}
            % this blank line is import for 2x2 subfigures, or replace it by \quad
\subfigure[40 views (refined)]{
\label{s_recon_40r}
\includegraphics[width=4cm,height=4cm]{statue_40v_rad_stat.png}}
\subfigure[57 views (refined)]{
\label{s_recon_50r}
\includegraphics[width=4cm,height=4cm]{statue_57v_rad_stat.png}}

\caption{The figures show 3D reconstruction of the statue scene using the 2-view SfM implementation for 40 and 57 views. (a) Unrefined reconstruction using 40 views. (b) Removed clustered artifacts using mass-centered sphere. (c) Additionally removed statistical outliers (refined). (d) Refined statue scene from all 57 views.}
\label{s_recon}
\end{figure}

Unlike in the fountain scene we can observe more clustered artifacts way out of the mass-center, which approximately coincides with the statue center. These cannot be removed by simply using the statistical removal tool from Open3D since this determines the outliers by looking at neighboring points, thus clusters will not be recognized as such outliers. Removing these clusters is done by using a threshold sphere and we are left with statistical errors as shown in Figure \ref{s_recon_40urr}. A further refinement step results in Figure \ref{s_recon_40r}, which still contains unwanted points floating near the statue. A better choice of parameters for statistical removal, like increasing the neighboring radius or decreasing the deviation threshold, might result in a cleaner scene. Nevertheless the statue is clearly visible. That is actually why we chose this view set: Unlike the 40 views set the complete set of 57 views result in a statue covered in artifacts even after refinement. In fact after 40 views more and more artifacts are introduced to the scene, which is why we opted for Figure \ref{s_recon_40r}. This is pretty much inline with the previous fountain scene, which also obtained artifacts with the last views. To get further insight we again plot the mean reprojection error for each view as shown in Figure \ref{s_err}. 

\begin{figure}[htb]
\centering
\includegraphics[width=8cm,height=5cm]{statue_mean_rpj_err.png}
\caption{Showing the mean reprojection error for each view of the statue scene in logarithmic scale. This is done for the unrefined and refined point clouds.}
\label{s_err}
\end{figure}

We notice that refinement has a more prominent effect compared to the fountain scene. Except a first little spike at the beginning one observes a general trend in which the mean reprojection error increases with each newly added view. For the most part the errors stay under 10 which is certainly not the best upper limit but clearly enough to get a solid reconstruction, as we see that no view till 40 drastically overshoots this limit. For a handful of views at the beginning we even measure errors below 1. The approximate last third of the views measure errors sometime significantly above 10 and are even nearing 100. This coincides with the appearance of artifacts within the statue as seen in Figure \ref{s_recon_50r}. Nevertheless these errors are no way near of those error spikes from the fountain scene. The reason why artifacts are clearly more visible in the statue scene are likely due to the larger amount of "bad" views, assuming that each view gives a similar number of points regardless of the scene. Again these larger errors a probably due to bad estimation of the fundamental matrices and the corresponding handling of in- and outliers.

Comparing both implementation with the same dataset one notices several difference, the first one being execution time. While running the reconstruction we had to wait around one hour for completion using the 2-view implementation, whereas a run with the multi-view implementation took no longer than 5 minutes. Both were run one the same run-of-the-mill hardware. This probably boils down to the use of our own triangulation in 2-view and the use of OpenCV triangulation on multi-view. Also we did not down sample the input images in 2-view as we did in multi-view. A further aspect might be inefficient data handling in 2-view and its output of intermediate results. Concentrating now on the visual aspects, one notices a quit obvious discrepancy in the RGB colors of the point cloud. The colors on 2-view are more dark and contain red patches that are not visible in the original images, whereas multi-view is more on the light side and thus also much closer to the original. Even though the color of 2-view seemed mostly good in the fountain case, according to these results we assume there to be a bug in color conversion or a slightly wrong mapping of the points to their respective views. Nevertheless, this seems like a trivial matter and the actual 3D reconstruction is of greater value.

To make a fair comparison we do have to compare the multi-view result with the 57 view scene of the 2-view. Clearly, the mutli-view result is much better and closer to the original since it is not overloaded with artifacts. It is actual hard to tell what lies beneath all does artifacts, it might actually be a competitive reconstruction but at this point these are only assumptions so we go back to the 40 view version. With this configuration both results, \ref{Fig.sub.6}  and \ref{s_recon_40r}, do in fact look quite similar, with the main differences being the lack of statistical outliers in multi-view and the bigger amount of points in 2-view. Also, even though not plotted, multi-view results in a average mean reprojection errors of under 10. This is comparable  to the best views in 2-view. Multi-view does not suffer from increasing errors with increasing number of views as 2-view does. Since multi-view is equipped with BA it actually takes all views into account regarding the fundamental matrix and thus the triangulation. On the other hand 2-view always uses only a set of two views and computes  inliers only for the newly added view disregarding the previous points and how these are in line with the newest view. So it comes as no surprise that the 2-view result actually got more points even though multi-view is more accurate. And it is the same reason we find those artifacts in Figure \ref{s_recon_50r}. Hence the quality in this case boils down to the method used for finding the right fundamental matrix across all views.

\section{Conclusions}
The Structure from Motion integrate the techniques of image feature detecting and matching, camera self-calibration, 3D points reconstruction and bundle adjustment, therefore it provides viable methods for building 3D models of objects, buildings and scenes. The key issues with the SfM algorithms are they are sometimes quite CPU and memory intensive, especially when processing high quality images and a large set of datasets. This limits SfM algorithms that it is difficult to do real-time reconstruction. By applying some accelerating techniques such as graphics library, GPU, downsampling, the progress of reconstruction usually can be finished in 1 minute. Thus it is also a effective algorithm for 3D reconstruction.

We observed a fact that when without applying efficient optimization methods, for example, outliers removal, bundle adjustment, the reprojection errors will accumulate quickly during the iterations of processing camera scenes. It cause the SfM algorithm requires a lot of auxiliary optimization techniques and puts forward high requirements for researchers and programmers.

Camera self-calibration is a reliable method for SfM algorithm. From our experiments, we saw  by using Kruppa’s equations and Sturm’s method as camera self-calibration, we got the result as better as the calibrated camera. But it still contain more outliers by estimating camera intrinsics, and those outliers are tend to give higher reprojection errors. This can be seen from comparing figure \ref{Fig.sub.2} and figure \ref{Fig.sub.5}. Thus it is convenient if we can obtain camera intrinsics directly from camera parameters for 3D reconstruction. Although SfM with camera self-calibration need optimization methods as auxiliaries, such auxiliaries does not occupy many computing resources.

The SfM algorithm is noise sensitive, we can see the applications of denoising showing up around every parts of SfM algorithm, such as feature points detecting and matching, compute fundamental matrix with RANSAC, PnP, triangulation and bundle adjustment. Strict denoising methods reduce a lot of valid 3D point clouds in the final result, that is one of the reasons the SfM algorithm is a sparse 3D reconstruction. It's usually hard to get a relatively dense 3D point clouds. Then it is also difficult to reconstruct surface of the object througt the sparse 3D point clouds. But it is still very useful for structure reconstruction and localization.

For future work, we could take up the methods in SfM algorithm and research its applications. It can be used as a guide and inspiration for future research. Perhaps it might be fruitful to compare and utilize this algorithm to other 3D reconstruction algorithms to achieve real-time dense reconstruction. For example, we could compare SfM algorithm to the binocular reconstruction algorithms such as SGBM algorithm \cite{SGBM} and BM algorithm \cite{BM}, and to RGBD reconstruction algorithms such as Kinect Fusion\cite{Kinectfusion}.



  Link to our project repository: \url{https://gitlab.com/xiaohemaikoo/cv-3d-reconstruction/-/tree/main/final}





{\small
\bibliographystyle{ieee_fullname}
\bibliography{egbib}
}



\end{document}
